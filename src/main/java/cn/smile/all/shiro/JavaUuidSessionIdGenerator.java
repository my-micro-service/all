package cn.smile.all.shiro;

import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import org.apache.shiro.session.Session;
import org.apache.shiro.session.mgt.eis.SessionIdGenerator;

import java.io.Serializable;

/**
 * @author smile-jt
 * @Created 2021/6/28 17:24
 */
public class JavaUuidSessionIdGenerator implements SessionIdGenerator {
    @Override
    public Serializable generateId(Session session) {
        return IdWorker.get32UUID();
    }
}
