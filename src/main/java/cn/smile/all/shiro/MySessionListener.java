package cn.smile.all.shiro;

import org.apache.shiro.session.Session;
import org.apache.shiro.session.SessionListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * @author smile-jt
 * @Created 2021/6/29 10:26
 */
@Component
public class MySessionListener implements SessionListener {

    private static final Logger log = LoggerFactory.getLogger(MySessionListener.class);

    @Override
    public void onStart(Session session) {
        //会话创建时触发
        log.info("[MySessionListener].[onStart] ------> Create Session：{}", session.getId());
    }

    @Override
    public void onExpiration(Session session) {
        //会话过期时触发
        log.info("[MySessionListener].[onExpiration] ------> Drop Session：{}", session.getId());
    }

    @Override
    public void onStop(Session session) {
        //退出/会话过期时触发
        log.info("[MySessionListener].[onStop] ------> Stop Session：{}", session.getId());
    }
}
