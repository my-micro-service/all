package cn.smile.all.shiro;

import cn.smile.all.utils.SerializableUtils;
import cn.smile.business.core.service.IShiroSessionService;
import cn.smile.commons.bean.domain.core.ShiroSession;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.apache.commons.collections.CollectionUtils;
import org.apache.shiro.cache.Cache;
import org.apache.shiro.session.Session;
import org.apache.shiro.session.mgt.ValidatingSession;
import org.apache.shiro.session.mgt.eis.CachingSessionDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * @author smile-jt
 * @Created 2021/6/28 17:24
 */
@SuppressWarnings("SpellCheckingInspection")
public class MySessionDAO extends CachingSessionDAO {
    private static Logger logger = LoggerFactory.getLogger(MySessionDAO.class);

    @Resource
    private IShiroSessionService shiroSessionService;
    private Cache<Serializable, Session> activeSessions;
    private static final String SESSION_ID_KEY = "session_id";

    @Override
    protected void doUpdate(Session session) {
        if (session instanceof ValidatingSession && !((ValidatingSession) session).isValid()) {
            return;
        }
        ShiroSession shiroSession = new ShiroSession(session.getId().toString(), SerializableUtils.serialize(session));
        shiroSession.setUpdateTime(LocalDateTime.now());
        QueryWrapper<ShiroSession> qw = new QueryWrapper<>();
        qw.eq(SESSION_ID_KEY, session.getId().toString());
        shiroSessionService.update(shiroSession, qw);
        this.cache(session, session.getId());
    }

    @Override
    protected void doDelete(Session session) {
        QueryWrapper<ShiroSession> qw = new QueryWrapper<>();
        qw.eq(SESSION_ID_KEY, session.getId().toString());
        shiroSessionService.remove(qw);
        this.uncache(session);
    }


    @Override
    protected Serializable doCreate(Session session) {
        Serializable sessionId = generateSessionId(session);
        assignSessionId(session, sessionId);
        ShiroSession shiroSession = new ShiroSession(sessionId.toString(), SerializableUtils.serialize(session));
        QueryWrapper<ShiroSession> qw = new QueryWrapper<>();
        qw.eq(SESSION_ID_KEY, session.getId().toString());
        shiroSessionService.saveOrUpdate(shiroSession, qw);
        this.cache(session, session.getId());
        return session.getId();
    }

    @Override
    protected Session doReadSession(Serializable sessionId) {
        String sessionStr = null;
        try {
            QueryWrapper<ShiroSession> qw = new QueryWrapper<>();
            qw.eq(SESSION_ID_KEY, sessionId);
            ShiroSession shiroSession = shiroSessionService.getOne(qw);
            if (shiroSession != null) {
                sessionStr = shiroSession.getSession();
            }
        } catch (Exception e) {
            logger.error("通过sessionId获取session数据为空!");
        }
        if (StringUtils.isEmpty(sessionStr)) {
            return null;
        }
        return SerializableUtils.deserialize(sessionStr);
    }

    @Override
    public Collection<Session> getActiveSessions() {
        Cache<Serializable, Session> cache = getActiveSessionsCacheLazy();
        if (cache != null) {
            Collection<Session> cacheSessions = cache.values();
            if (CollectionUtils.isNotEmpty(cacheSessions)) {
                int count = shiroSessionService.count();
                if (count != cacheSessions.size()) {
                    List<ShiroSession> sessionList = shiroSessionService.list();
                    sessionList.forEach(shiroSession -> {
                        Session s = SerializableUtils.deserialize(shiroSession.getSession());
                        cache(s, shiroSession.getId());
                    });
                } else {
                    return cache.values();
                }
            } else {
                List<ShiroSession> sessionList = shiroSessionService.list();
                sessionList.forEach(shiroSession -> {
                    Session s = SerializableUtils.deserialize(shiroSession.getSession());
                    cache(s, shiroSession.getId());
                });
            }
            cache = getActiveSessionsCacheLazy();
            return cache.values();
        } else {
            return Collections.emptySet();
        }
    }

    private Cache<Serializable, Session> getActiveSessionsCacheLazy() {
        if (this.activeSessions == null) {
            this.activeSessions = createActiveSessionsCache();
        }
        return activeSessions;
    }
}
