package cn.smile.all.shiro;

import cn.smile.business.core.service.ICoreUserService;
import cn.smile.business.core.service.IPrivilegeAclService;
import cn.smile.business.core.service.IPrivilegeGroupService;
import cn.smile.commons.bean.dto.core.CoreUserDTO;
import cn.smile.commons.bean.dto.core.ModulePermissionDTO;
import cn.smile.commons.bean.dto.core.PrivilegeGroupDTO;
import cn.smile.commons.constant.NumberConstant;
import cn.smile.commons.response.ResponseCode;
import com.alibaba.fastjson.JSON;
import org.apache.commons.collections.CollectionUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * @author smile-jt
 * @Created 2021/6/28 10:22
 */
public class AuthRealm extends AuthorizingRealm {

    private static final Logger log = LoggerFactory.getLogger(AuthRealm.class);

    @Resource
    private ICoreUserService userService;
    @Resource
    private IPrivilegeAclService aclService;
    @Resource
    private IPrivilegeGroupService groupService;

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        log.info("[AuthRealm].[doGetAuthorizationInfo] ------> Do Get Authorization Info Start, principals = {}", JSON.toJSONString(principals));
        CoreUserDTO user = (CoreUserDTO) principals.fromRealm(getName()).iterator().next();
        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
        List<String> permissions = new ArrayList<>();
        Set<ModulePermissionDTO> modulePermissions = aclService.getModulePermissionsByUserId(user.getId(), user.getUserType());
        modulePermissions.forEach(mp -> permissions.add(mp.getModuleSn() + ":" + mp.getPermissionValue()));
        info.addStringPermissions(permissions);
        List<String> groups = new ArrayList<>();
        List<PrivilegeGroupDTO> groupList = groupService.getGroupsByUserId(user.getId());
        if (CollectionUtils.isNotEmpty(groupList)) {
            groupList.forEach(group -> groups.add(group.getSn()));
        }
        info.addRoles(groups);

        log.info("[AuthRealm].[doGetAuthorizationInfo] ------> Do Get Authorization Info End, info = {}", JSON.toJSONString(info));
        return info;
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
        log.info("[AuthRealm].[doGetAuthenticationInfo] ------> Do Get Authentication Info Start, token = {}", JSON.toJSONString(token));
        UsernamePasswordToken userToken = (UsernamePasswordToken) token;
        String username = userToken.getUsername();
        CoreUserDTO user = userService.getUserByUsername(username);
        if (Objects.isNull(user)) {
            throw new UnknownAccountException();
        } else {
            if (Objects.isNull(user.getStatus()) || NumberConstant.ZERO == user.getStatus()) {
                throw new UnknownAccountException(ResponseCode.USER_ACCOUNT_FORBIDDEN.message());
            }
        }
        SimpleAuthenticationInfo info = new SimpleAuthenticationInfo(user, user.getPassword(), getName());
        log.info("[AuthRealm].[doGetAuthenticationInfo] ------> Do Get Authentication Info End, info = {}", JSON.toJSONString(info));
        return info;
    }
}
