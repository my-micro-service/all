package cn.smile.all.shiro;

import cn.smile.all.utils.SerializableUtils;
import cn.smile.business.core.service.IShiroSessionService;
import cn.smile.commons.bean.domain.core.ShiroSession;
import org.apache.commons.collections.CollectionUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.session.mgt.SimpleSession;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * @author smile-jt
 * @Created 2021/6/29 10:18
 */
@SuppressWarnings("SpellCheckingInspection")
@Configuration
@EnableScheduling
public class SessionScheduleTask {
    @Resource
    private MySessionDAO mySessionDao;
    @Resource
    private IShiroSessionService sessionService;

    @Scheduled(fixedRate = 1000 * 60 * 60)
    private void configureTasks() {
        List<ShiroSession> list = sessionService.list();
        if (CollectionUtils.isNotEmpty(list)){
            list.forEach(shiroSession -> {
                Session session = SerializableUtils.deserialize(shiroSession.getSession());
                SimpleSession simpleSession = (SimpleSession) session;
                if (simpleSession.getStopTimestamp() != null){
                    mySessionDao.doDelete(simpleSession);
                }
                long time = new Date().getTime() - simpleSession.getLastAccessTime().getTime();
                if (time > simpleSession.getTimeout()) {
                    mySessionDao.doDelete(simpleSession);
                }
            });
        }
    }
}
