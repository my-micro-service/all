package cn.smile.all.shiro;

import cn.smile.commons.constant.CommonConstant;
import cn.smile.commons.utils.Md5Util;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authc.credential.SimpleCredentialsMatcher;

/**
 * @author smile-jt
 * @Created 2021/6/29 10:11
 */
public class MyCredentialsMatcher extends SimpleCredentialsMatcher {

    @Override
    public boolean doCredentialsMatch(AuthenticationToken token, AuthenticationInfo info) {
        UsernamePasswordToken uToken = (UsernamePasswordToken) token;
        String inPassword = new String(uToken.getPassword());
        inPassword = Md5Util.getMD5String(CommonConstant.MD5_PREFIX + inPassword);
        String dbPassword = (String) info.getCredentials();
        return this.equals(inPassword, dbPassword);
    }
}
