package cn.smile.all.controller;

import cn.smile.business.core.service.IOperatingRecordService;
import cn.smile.business.core.service.IPrivilegeAclService;
import cn.smile.commons.bean.domain.core.OperatingRecord;
import cn.smile.commons.bean.dto.core.CoreUserDTO;
import cn.smile.commons.bean.dto.core.PrivilegeAclDTO;
import cn.smile.commons.bean.form.dashboard.login.LoginForm;
import cn.smile.commons.constant.CommonConstant;
import cn.smile.commons.enums.SourceEnum;
import cn.smile.commons.response.ResponseCode;
import cn.smile.commons.response.ResponseResult;
import cn.smile.commons.utils.FastJsonUtils;
import cn.smile.commons.utils.IpUtils;
import com.alibaba.fastjson.JSON;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.ExcessiveAttemptsException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * @author smile-jt
 * @Created 2021/6/29 13:41
 */
@SuppressWarnings("SpellCheckingInspection")
@RestController
@RequestMapping("/")
public class LoginController {
    private static final Logger log = LoggerFactory.getLogger(LoginController.class);

    @Resource
    private IPrivilegeAclService aclService;
    @Resource
    private IOperatingRecordService operatingRecordService;

    @PostMapping(value = "login")
    public ResponseResult login(HttpServletResponse response, HttpServletRequest request, @RequestBody LoginForm form) {
        log.info("[LoginController].[login] ------> User Login Start, form = {}", JSON.toJSONString(form));
        if (Objects.isNull(form) || StringUtils.isEmpty(form.getUsername()) || StringUtils.isEmpty(form.getPassword())) {
            return ResponseResult.failure(ResponseCode.USERNAME_OR_PASSWORD_IS_NULL);
        }

        //调用shiro校验用户名及密码正确性
        UsernamePasswordToken usernamePasswordToken = new UsernamePasswordToken(form.getUsername(), form.getPassword());
        Subject subject = SecurityUtils.getSubject();
        try {
            subject.login(usernamePasswordToken);
        } catch (UnknownAccountException | IncorrectCredentialsException e) {
            return ResponseResult.failure(ResponseCode.USER_LOGIN_ERROR);
        } catch (ExcessiveAttemptsException e) {
            return ResponseResult.failure(ResponseCode.USER_LOCK);
        } catch (Exception e) {
            log.error("[LoginController].[login] ------> error:", e);
            return ResponseResult.failure(ResponseCode.UNKNOWN);
        }

        //登录成功
        CoreUserDTO user = (CoreUserDTO) subject.getPrincipal();
        String sessionId = (String) subject.getSession().getId();
        response.setHeader(CommonConstant.DRAGON_SESSION_ID, sessionId);
        response.setHeader(CommonConstant.FRONT_OR_BACK, CommonConstant.REQUEST_ADMIN);
        this.setSessionInfo(user, subject, request);
        log.info("[LoginController].[login] ------> User Login End, sessionId = {}", sessionId);
        return ResponseResult.success((Object) sessionId);
    }

    /**
     * 设置session信息
     *
     * @param user    用户信息
     * @param subject 登录信息
     * @param request 请求对象
     */
    private void setSessionInfo(CoreUserDTO user, Subject subject, HttpServletRequest request) {
        Session session = subject.getSession();
        if (!Objects.isNull(user)) {
            session.setAttribute(CommonConstant.LOGIN_USER, user);
            Set<PrivilegeAclDTO> aclList = aclService.getAclSetByUserId(user.getId());
            // 把用户拥有的所有权限ACL列表查询出来放到session中
            session.setAttribute(CommonConstant.LOGIN_USER_ACL, aclList);
            Map<String, String> content = new HashMap<>();
            content.put("msg", "登录");
            OperatingRecord record = OperatingRecord.builder()
                    .ip(IpUtils.getRemoteIP(request))
                    .userCode(user.getId().toString())
                    .userName(user.getNickname())
                    .source(SourceEnum.ADMIN.getSn())
                    .operatingContent(FastJsonUtils.objectToJson(content))
                    .operatingType(request.getMethod())
                    .dateTime(LocalDateTime.now())
                    .build();
            operatingRecordService.save(record);
        }
    }

    /**
     * 用户登出
     */
    @PostMapping(value = "/logout", produces = "application/json")
    public ResponseResult logout() {
        Subject currentUser = SecurityUtils.getSubject();
        currentUser.logout();
        return ResponseResult.success(ResponseCode.LOGIN_OUT_SUCCESS);
    }
}
