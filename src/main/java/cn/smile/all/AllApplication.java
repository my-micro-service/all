package cn.smile.all;

import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * <p>
 * 快速上线,打包单体应用
 * </p>
 *
 * @author 龙逸
 * @since 2020/08/19
 */
@EnableScheduling
@EnableTransactionManagement
@SpringBootApplication(scanBasePackages = {"cn.smile"})
@MapperScan(basePackages = "cn.smile.repository.**.mapper")
public class AllApplication {

    private static final Logger logger = LoggerFactory.getLogger(AllApplication.class);

    /**
     * MyBatis Plus 分页插件
     *
     * @return {@link PaginationInterceptor}
     */
    @Bean
    public PaginationInterceptor paginationInterceptor() {
        return new PaginationInterceptor();
    }

    public static void main(String[] args) {
        try {
            SpringApplication.run(AllApplication.class, args);
            logger.info("---------------------AllApplication Successful Start---------------------");
        }catch (Exception e){
            logger.error("AllApplication Failure Start, e:", e);
        }
    }
}
